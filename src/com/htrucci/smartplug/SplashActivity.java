package com.htrucci.smartplug;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class SplashActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.splash);
		final EditText id = (EditText) findViewById(R.id.editId);
		final EditText pass = (EditText) findViewById(R.id.editPass);
		final Button login = (Button) findViewById(R.id.loginBtn);

		login.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (id.getText().toString().equals("admin") && pass.getText().toString().equals("1234")) {
					finish();
				}
			}
		});

	}
}
