package com.htrucci.smartplug;

import java.io.IOException;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import android.widget.ToggleButton;

@TargetApi(Build.VERSION_CODES.GINGERBREAD)
public class NewClient extends Activity implements OnClickListener{
	
    private String ip = "162.243.140.49"; // 필요없음(서버를 IP로 접속할시)
    private String domain = "192.168.0.100"; // 고정 도메인으로 서버IP를 받아올 것임.
    private int port = 9090; // 서버 PORT번호
    public static SocketManager socket = SocketManager.getInstance(); 
    private Object ob;
 
    @Override
	protected void onResume() { // 첫실행시, 액티비티 재시작시 서버로부터 초기화
		// TODO Auto-generated method stub
		super.onResume();
		socket.openSocket(ip, port);
		socket.sendMsg("init");
		ob = socket.readObj();
        tv[0].setText(ob.getName1());
        tv[1].setText(ob.getName2());
        tv[2].setText(ob.getName3());
        tb[0].setChecked(ob.getNum1());
        tb[1].setChecked(ob.getNum2());
        tb[2].setChecked(ob.getNum3());
	}

    final TextView tv[] = new TextView[3];
    final ToggleButton tb[] = new ToggleButton[3];
    Button setupBtn;
    int i=0;
    int j=0;
    @Override
	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        startActivity(new Intent(this, SplashActivity.class));
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main);
        StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().permitNetwork().build());

/*        
        tabhost.addTab(tabhost.newTabSpec("tab1").setIndicator("탭1").setContent(new Intent(this,SetupActivity.class)));
        tabhost.addTab(tabhost.newTabSpec("tab2").setIndicator("탭2").setContent(new Intent(this,SetupActivity.class)));
        */
        //mHandler = new Handler();
        tv[0] = (TextView)findViewById(R.id.TextView01);
        tv[1] = (TextView)findViewById(R.id.TextView02);
        tv[2] = (TextView)findViewById(R.id.TextView03);
        tb[0] = (ToggleButton)findViewById(R.id.toggle1);
        tb[1] = (ToggleButton)findViewById(R.id.toggle2);
        tb[2] = (ToggleButton)findViewById(R.id.toggle3);
        setupBtn = (Button) findViewById(R.id.SetupBtn);
        
        for(int m=0; m<3; m++){
        	tb[m].setOnClickListener(this);
        }
	    //checkUpdate.start(); // 클라이언트 응답값 체크 쓰레드
	    
        setupBtn.setOnClickListener(new OnClickListener() { // 상세설정 버튼
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(NewClient.this, SetupActivity.class);
				startActivity(intent);
			}
		});
}
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if(v.getId()==R.id.toggle1){ // 1번 토글 누를시 토글ID 설정 및 기능실행
			i=0;
			j=1;
			toggleCheck(); 
			}
		else if(v.getId()==R.id.toggle2){ // 2번 토글 누를시 토글ID 설정 및 기능실행
			i=1;
			j=2;
			toggleCheck();
		}
		else if(v.getId()==R.id.toggle3){ // 3번 토글 누를시 토글ID 설정 및 기능실행
			i=2;
			j=3;
			toggleCheck();
		}
	}
	public void toggleCheck(){
		if(tb[i].isChecked()){ // 토글이 TRUE 일때
			//socket = new SocketManager(ip, port);
			socket.openSocket(ip, port);
			socket.sendMsg("Device"+(i+1)+"_ON");
		}else if(tb[i].isChecked()==false){ // 토글이 FALSE 일떄
			//socket = new SocketManager(ip, port);
			socket.openSocket(ip, port);
			socket.sendMsg("Device"+(i+1)+"_OFF");
		}
	}
    @Override
	protected void onStop() { // 액티비티 닫을때 소켓 닫기
        super.onStop();
        try {
			socket.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
}