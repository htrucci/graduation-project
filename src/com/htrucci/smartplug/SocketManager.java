package com.htrucci.smartplug;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.StreamCorruptedException;
import java.net.Socket;
import java.net.UnknownHostException;

import android.util.Log;

public class SocketManager {

    public static Socket socket;
    private BufferedReader networkReader;
    private BufferedWriter networkWriter;
    private static InputStream is = null;
    private static OutputStream os = null;
    private static ObjectInputStream ois = null;
    private static ObjectOutputStream oos = null;
    private static Object ob;
    private static SocketManager s_instance;
	
	public static SocketManager getInstance(){
		
		if(s_instance == null)
			s_instance = new SocketManager();
		return s_instance;
	}
	public void openSocket(String ip, int port) {
		try {
			socket = new Socket(ip, port);
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			Log.e("err", "text : "+e.getMessage());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			Log.e("err", "text : "+e.getMessage());
		}
	}

	public Object readObj() {
        try {
			is = socket.getInputStream();
			ois = new ObjectInputStream(is);
	   	 	ob = (Object) ois.readObject();
		} catch (StreamCorruptedException e) {
			// TODO Auto-generated catch block
			Log.e("SocketManager.java errst", "text : "+e.getMessage());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			Log.e("SocketManager.java errio", "text : "+e.getMessage());
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			Log.e("SocketManager.java errclassnot", "text : "+e.getMessage());
		}
		return ob;
	}
	public void writeObj(Object obj) throws IOException, ClassNotFoundException{
		os = socket.getOutputStream();
		oos = new ObjectOutputStream(os);
		oos.writeObject(ob);
	}
	public void sendMsg(String msg) {
		try {
			networkWriter = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
	        networkReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		PrintWriter out = new PrintWriter(networkWriter, true);
        out.println(msg); // ���۾ƿ����� msg ���� 
	}
	public void close() throws IOException
	{
		socket.close();
	}

}
