package com.htrucci.smartplug;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.EditText;


public class ChangeNameDialog extends Dialog implements OnTouchListener {
	
	private EditText changeName;
	private Button addOK;
	private String _changeName;
	private int num;
	public ChangeNameDialog(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}
	public String getName(){
		return _changeName;
	}
	public void setNum(int n){
		num = n;
	}
	public int getNum(){
		return num;
	}
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.namedialog);
		changeName = (EditText)findViewById(R.id.changeName);
		addOK = (Button)findViewById(R.id.addOK);
		addOK.setOnTouchListener(this);
	}
	
	@Override
	public boolean onTouch(View v, MotionEvent event) {
		// TODO Auto-generated method stub
		if(v == addOK){
			_changeName = changeName.getText().toString();
		}
		dismiss();

		return false;
	}
	

}
