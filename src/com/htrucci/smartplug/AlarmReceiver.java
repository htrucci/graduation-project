package com.htrucci.smartplug;

import java.io.IOException;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

public class AlarmReceiver extends BroadcastReceiver {

	SocketManager socket = SocketManager.getInstance();
    private String ip = "162.243.140.49"; // 필요없음(서버를 IP로 접속할시)
    private int port = 9090; // 서버 PORT번호
	@Override
	public void onReceive(Context arg0, Intent arg1) {
		// TODO Auto-generated method stub
		int devNum[] = arg1.getIntArrayExtra("dev");
		String msg = "";
		socket.openSocket(ip, port);
		switch(devNum[0])
		{
			case 1:
				msg += "Device1_";
				break;
			case 2:
				msg += "Device2_";
				break;
			case 3:
				msg += "Device3_";
				break;
			default:
				break;
		}
		switch(devNum[1])
		{
			case 0:
				msg += "OFF";
				break;
			case 1:
				msg += "ON";
				break;
		}
		Toast.makeText(arg0, msg, Toast.LENGTH_SHORT).show();
		socket.sendMsg(msg);
		AutomationActivity.h[devNum[0]]=25;
		AutomationActivity.m[devNum[0]]=0;
		try {
			socket.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
