package com.htrucci.smartplug;

import java.io.Serializable;

public class Object implements Serializable{
	private static final long serialVersionUID = 1L;
	// 디바이스의 이름과 상태를 저장하고 있는 메소드
	// 한글 전송을 위해 굳이 객체를 사용함
	private String Device1_name = "AAA";
	private String Device2_name = "BBB";
	private String Device3_name = "CCC";
	private boolean Device1_status=false;
	private boolean Device2_status=false;
	private boolean Device3_status=false;
	
	public void setNum1(boolean n){
		Device1_status = n;
	}
	public void setNum2(boolean n){
		Device2_status = n;
	}
	public void setNum3(boolean n){
		Device3_status = n;
	}
	public void setName(String name, int num){
		if(num==0)
			Device1_name = name;
		if(num==1)
			Device2_name = name;
		if(num==2)
			Device3_name = name;
	}
	public String getName1(){
		return Device1_name;
	}
	public String getName2(){
		return Device2_name;
	}
	public String getName3(){
		return Device3_name;
	}
	public boolean getNum1(){
		return Device1_status;
	}
	public boolean getNum2(){
		return Device2_status;
	}
	public boolean getNum3(){
		return Device3_status;
	}	
	public void print(){
	}
}
