package com.htrucci.smartplug;

import java.io.IOException;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;
 
public class SetupActivity extends Activity implements OnClickListener{

    //private String html = "";
    //private Handler mHandler;
    private String ip = "162.243.140.49"; // 필요없음(서버를 IP로 접속할시)
    private int port = 9090; // 서버 PORT번호
    /*private String domain = "192.168.0.100"; // 고정 도메인으로 서버IP를 받아올 것임.
    
    private Socket socket; 
    private BufferedReader networkReader;
    private BufferedWriter networkWriter;
	static InputStream is = null;
	static OutputStream os = null;
	static ObjectInputStream ois = null;
	static ObjectOutputStream oos = null;*/
	public static SocketManager socket = SocketManager.getInstance();
    final TextView tv[] = new TextView[3];
    final ToggleButton tb[] = new ToggleButton[3];
    final Button nameBtn[] = new Button[3];
    final Button autoBtn[] = new Button[3];
    static Object ob;
    int i=0;
    int j=0;
    ChangeNameDialog changenamedialog;
	@Override
	@TargetApi(Build.VERSION_CODES.GINGERBREAD)    
	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.status);
        socket.sendMsg("init");
        tv[0] = (TextView)findViewById(R.id.TextView01);
        tv[1] = (TextView)findViewById(R.id.TextView02);
        tv[2] = (TextView)findViewById(R.id.TextView03);
        tb[0] = (ToggleButton)findViewById(R.id.toggle1);
        tb[1] = (ToggleButton)findViewById(R.id.toggle2);
        tb[2] = (ToggleButton)findViewById(R.id.toggle3);
        nameBtn[0] = (Button)findViewById(R.id.nameBtn1);
        nameBtn[1] = (Button)findViewById(R.id.nameBtn2);
        nameBtn[2] = (Button)findViewById(R.id.nameBtn3);
        autoBtn[0] = (Button)findViewById(R.id.autoBtn1);
        autoBtn[1] = (Button)findViewById(R.id.autoBtn2);
        autoBtn[2] = (Button)findViewById(R.id.autoBtn3);
        StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().permitNetwork().build());

        for(int m=0; m<3; m++){
        	tb[m].setOnClickListener(this);
        }

        for(int m=0; m<3; m++){
        	tb[m].setOnClickListener(this);
            nameBtn[m].setOnClickListener(this);
            autoBtn[m].setOnClickListener(this);
        }
        if(changenamedialog==null) // 이름변경 다이얼로그 생성
        	changenamedialog = new ChangeNameDialog(SetupActivity.this);
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		socket.openSocket(ip, port);
		socket.sendMsg("init");
		ob = socket.readObj();
        tv[0].setText(ob.getName1());
        tv[1].setText(ob.getName2());
        tv[2].setText(ob.getName3());
        tb[0].setChecked(ob.getNum1());
        tb[1].setChecked(ob.getNum2());
        tb[2].setChecked(ob.getNum3());
        
        //socket.outStreamClose();
       // socket.inStreamClose();
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId())
		{
			case R.id.toggle1: // 1번 토글 누를시 토글ID 설정 및 기능실행
				i=0;
				j=1;
				toggleCheck();
				break;
			case R.id.toggle2: // 2번 토글 누를시 토글ID 설정 및 기능실행
				i=1;
				j=2;
				toggleCheck();
				break;
			case R.id.toggle3: // 3번 토글 누를시 토글ID 설정 및 기능실행
				i=2;
				j=3;
				toggleCheck();
				break;
			case R.id.nameBtn1: // 커스텀 다이얼로그로 이름 변경 
				changenamedialog = new ChangeNameDialog(SetupActivity.this);
				changenamedialog.setTitle("1이름변경");
				changenamedialog.setNum(0);
				changenamedialog.show();
				break;
			case R.id.nameBtn2: // 커스텀 다이얼로그로 이름 변경
				changenamedialog = new ChangeNameDialog(SetupActivity.this);
				changenamedialog.setTitle("2이름변경");
				changenamedialog.setNum(1);
				changenamedialog.show();
				break;
			case R.id.nameBtn3: // 커스텀 다이얼로그로 이름 변경
				changenamedialog = new ChangeNameDialog(SetupActivity.this);
				changenamedialog.setTitle("3이름변경");
				changenamedialog.setNum(2);
				changenamedialog.show();
				break;
			case R.id.autoBtn1:
				Intent intent = new Intent(this, AutomationActivity.class);
				intent.putExtra("dev", 1);
				startActivity(intent);
				break;
			case R.id.autoBtn2:
				intent = new Intent(this, AutomationActivity.class);
				intent.putExtra("dev", 2);
				startActivity(intent);
				break;
			case R.id.autoBtn3:
				intent = new Intent(this, AutomationActivity.class);
				intent.putExtra("dev", 3);
				startActivity(intent);
				break;
			default:
				break;
		}
		 changenamedialog.setOnDismissListener(new OnDismissListener() { // 이름변경시 처리루틴
				@Override
				public void onDismiss(DialogInterface dialog) {
					// TODO Auto-generated method stub
					int n = changenamedialog.getNum();
					tv[n].setText(changenamedialog.getName());
					ob.setName(tv[n].getText().toString(), n);
					Toast.makeText(SetupActivity.this, n + "이름바꾸기 성공", Toast.LENGTH_SHORT).show();
					try {
						socket.openSocket(ip, port);
						socket.sendMsg("changename");
						socket.writeObj(ob);
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} catch (ClassNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
				}	
			});
	}
	public void toggleCheck(){ 
		if(tb[i].isChecked()){ // 토글이 TRUE 일떄
			socket.openSocket(ip, port);
	        socket.sendMsg("Device"+(i+1)+"_ON"); // 버퍼네트워크 소켓으로 "Device(i+1)_ON" 전송
		}else if(tb[i].isChecked()==false){ // 토글이 FALSE 일때
			socket.openSocket(ip, port);
			socket.sendMsg("Device"+(i+1)+"_OFF");
		}
	}
	/*@Override
	protected void onStop() { // 액티비티 닫을때 소켓 닫기
        super.onStop();
        try {
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    private Thread checkUpdate = new Thread() { // 서버로부터 응답값 체크 쓰레드
        @Override
		public void run() {
            try {
                String line;
                Log.w("GDClient", "Start Thread");
                Thread.sleep(2000);
                while (true) {
                	Thread.sleep(2000);
                    Log.w("GDClient", "Client is running");
                    line = networkReader.readLine();
                    html = line;
                    mHandler.post(showUpdate);
                }
            } catch (Exception e) {
            	Log.w("GDClient", "Start Thread ERR"+e);
            }
        }
    };
 
    private Runnable showUpdate = new Runnable() { // 쓰레드에서 컴포넌트 접근못해서 해결하기위해 있음
 
		@Override
		public void run() {
			try{
			Log.w("GDClient", "showUpdate()");
			if(html==null){
				Log.w("GDClient", "Coming Word: null");
			}else
			Toast.makeText(SetupActivity.this, "서버: " + html + "정상처리됨.", Toast.LENGTH_SHORT).show();
			}catch(Exception e){
				e.printStackTrace();
			}
		}
    };
    public void init(String ip, int port) throws IOException, ClassNotFoundException{
   	 try {
  		 setSocket(ip, port);  // 소켓연결
         Toast.makeText(getApplication(), "Server ip:" + ip, Toast.LENGTH_SHORT).show();             
         PrintWriter out = new PrintWriter(networkWriter, true);
         out.println("init"); // 버퍼아웃으로 "init" 보냄 

         // 객체 전송받기 위한 설정
         is = socket.getInputStream();
         ois = new ObjectInputStream(is);
         ob = (Object) ois.readObject();
         // 받은 객체로 이름 및 상태 초기화
         tv[0].setText(ob.getName1());
       	 tv[1].setText(ob.getName2());
       	 tv[2].setText(ob.getName3());
       	 tb[0].setChecked(ob.getNum1());
       	 tb[1].setChecked(ob.getNum2());
       	 tb[2].setChecked(ob.getNum3());
    	 // 모든 소켓 닫기
         ois.close();
         is.close();
         socket.close();
         Toast.makeText(SetupActivity.this, "서버에서 불러오기 완료.", Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            System.out.println(e);
            e.printStackTrace();
        }  	 
   }
    public void setSocket(String ip, int port) throws IOException {
    	
        try {
        	//InetAddress address = InetAddress.getByName(ip); // 도메인으로부터 정보 받아오기
        	//ip = address.getHostAddress(); // 받아온 정보로부터 IP만 빼기
            socket = new Socket(ip, port); // 소켓 초기화
            networkWriter = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
            networkReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        } catch (IOException e) {
            System.out.println(e);
            e.printStackTrace();
        }
    }*/
}