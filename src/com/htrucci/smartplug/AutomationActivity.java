package com.htrucci.smartplug;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.app.TimePickerDialog.OnTimeSetListener;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

public class AutomationActivity extends Activity {
	
    private String ip = "162.243.140.49"; // 필요없음(서버를 IP로 접속할시)
    private int port = 9090; // 서버 PORT번호
    public static int[] h={0,25,25,25};
    public static int[] m=new int[4];
    public static String[] ap=new String[4];
    static Object ob;
    int devIdx;
    TextView tV;
    CustomListAdapter myAdpt;
    String[] option;
    ListView lV;
    TimePickerDialog timeDialog;
    TextView timeText;
    int sh;
    public static SocketManager socket = SocketManager.getInstance();
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.auto_setting);
		Intent intent = getIntent();
		devIdx = intent.getIntExtra("dev",  -1);
		lV = (ListView)findViewById(R.id.listView1);
		
		ArrayList<String[]> list = new ArrayList<String[]>();
		option = new String[2];
		option[0] = "자동 켜짐 시각";
		option[1] = "0분 후";
		if(AutomationActivity.h[devIdx] < 25)
			option[1] = AutomationActivity.ap+
			Integer.toString(AutomationActivity.h[devIdx])+"시 "+
			Integer.toString(AutomationActivity.m[devIdx])+"분";
			
		list.add(option);
		option = new String[2];
		option[0] = "자동 꺼짐 시각";
		option[1] = "0분 후";
		list.add(option);
		myAdpt = new CustomListAdapter(this, R.layout.listv_layout, list);
		//optionText.setText("자동 켜짐 시각");
		//option.add(optionText);
		//option.add("!!");
		lV.setAdapter(myAdpt);
		lV.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View v, int position,
			long id) {
				/*LinearLayout _v = (LinearLayout)v;
				LinearLayout __v = ((LinearLayout)_v.getChildAt(0));*/
				Calendar c = Calendar.getInstance();
				c.setTimeInMillis(System.currentTimeMillis());
				timeDialog = new TimePickerDialog(AutomationActivity.this, timeL, 
						c.get(Calendar.HOUR_OF_DAY), c.get(Calendar.MINUTE), false);
				timeDialog.setTitle("시각 설정");
				timeText = (TextView)v.findViewById(R.id.optionS);
				timeDialog.show();
				sh = position;
				//Toast.makeText(getApplicationContext(), timeText.getText(), Toast.LENGTH_SHORT).show();
				//myAdpt.notifyDataSetChanged();
			}
			
		});
		tV = (TextView)findViewById(R.id.dv_name);
		
		socket.openSocket(ip, port);
		socket.sendMsg("init");
		ob = socket.readObj();

		switch(devIdx)
		{
			case 1:
				tV.setText(ob.getName1());
				break;
			case 2:
				tV.setText(ob.getName2());
				break;
			case 3:
				tV.setText(ob.getName3());
				break;
			default:
				break;
		}

		/*tV.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				int a[] = {1, 1};
				Intent intent = new Intent(getApplicationContext(), AlarmReceiver.class);
				intent.putExtra("dev", a);
		        PendingIntent sender = PendingIntent.getBroadcast(getApplicationContext(), 0, intent, 0);

		        Calendar calendar = Calendar.getInstance();

		        calendar.setTimeInMillis(System.currentTimeMillis());

		        calendar.add(Calendar.SECOND, 5);

		        AlarmManager am = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
		        am.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), sender); 
			}
		});*/
		   
	}
	
    private OnTimeSetListener timeL = new OnTimeSetListener() {
		@Override
		public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
			// TODO Auto-generated method stub
			String s = "";
			int dev[] = new int[2];
			if(hourOfDay > 12) {
				s += "오후 ";
				s += Integer.toString(hourOfDay-12);
				s += "시 ";
		        AutomationActivity.h[devIdx] = hourOfDay-12;
		        AutomationActivity.ap[devIdx] = "오후 ";
			}
			else {
				s += "오전 ";
				s += Integer.toString(hourOfDay);
				s += "시 ";
				AutomationActivity.ap[devIdx] = "오전 ";
		        AutomationActivity.h[devIdx] = hourOfDay;
			}
	        AutomationActivity.m[devIdx] = minute;
			if(sh == 0) {
				dev[0] = devIdx;
				dev[1] = 1;
			}
			else {
				dev[0] = devIdx;
				dev[1] = 0;
			}
			int reqCode = devIdx*10 + dev[1];
			Intent intent = new Intent(getApplicationContext(), AlarmReceiver.class);
			intent.putExtra("dev", dev);
			//intent.putExtra
			PendingIntent pi = PendingIntent.getBroadcast(getApplicationContext(), reqCode, intent, 0);
			//PendingIntent pi2 = PendingIntent.getBroadcast(getApplicationContext(), 1, intent2, 0);
			
	        Calendar calendar = Calendar.getInstance();
	        calendar.setTimeInMillis(System.currentTimeMillis());
	        calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
	        calendar.set(Calendar.MINUTE, minute);
	        calendar.set(Calendar.SECOND, 0);
	        calendar.set(Calendar.MILLISECOND, 0);
	        AlarmManager am = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
	        am.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pi);
	        //am.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis()+10000, pi2);
			s += Integer.toString(minute)+"분";
			timeText.setText(s);
		}
    };
}
