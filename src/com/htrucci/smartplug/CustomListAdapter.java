package com.htrucci.smartplug;

import java.util.ArrayList;

import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;



public class CustomListAdapter extends ArrayAdapter<ArrayList<String>> {

	Activity context;
	ArrayList<String[]> item;
	int i = 0;
	CustomListAdapter(Activity context, int resID, ArrayList<String[]> list) {
		super(context, resID);
		this.context = context;
		this.item = list;
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return item.size();
	}

	public View getView(int position, View convertView, ViewGroup parent)
	{
		View row = convertView;
		if(row == null) {
			LayoutInflater inflater = context.getLayoutInflater();
			row = inflater.inflate(R.layout.listv_layout,  null);
		}
		TextView list = (TextView)row.findViewById(R.id.optionT);
		TextView sub = (TextView)row.findViewById(R.id.optionS);
		list.setText(item.get(position)[0]);
		sub.setText(item.get(position)[1]);
		Log.i("called", "time : "+(++i));
		return row;
	}
}